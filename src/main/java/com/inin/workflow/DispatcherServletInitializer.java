package com.inin.workflow;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class DispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
        logger.info("getServletConfigClasses()");
        return new Class<?>[] {WebConfig.class};
	}

	@Override
	protected String[] getServletMappings() {
        logger.info("getServletMappings()");
        return new String[] { "/" };
	}

}
