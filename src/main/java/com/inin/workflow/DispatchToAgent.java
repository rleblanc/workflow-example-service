package com.inin.workflow;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.log4j.Logger;

public class DispatchToAgent implements JavaDelegate {

	private Logger logger = Logger.getLogger("com.inin.workflow");
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {
    	logger.info("Entering DispatchToAgent");
    	HomeController.addOutputLine("Entering", "DispatchToAgent.execute");
	}

}
