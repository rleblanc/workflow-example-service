package com.inin.workflow;

import java.io.IOException;

import javax.servlet.ServletException;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.servlet.WebappContext;

// import com.test.hello.DispatcherServletInitializer;

public class WebServer {

	public static void main(String[] args) {
		HttpServer server = HttpServer.createSimpleServer("/", 8080);
		try {
			server.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		DispatcherServletInitializer initializer = new DispatcherServletInitializer();
		WebappContext ctx = new WebappContext("WebappContext");
		
		try {
			initializer.onStartup(ctx);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ctx.deploy(server);
		
		try {
			Thread.currentThread().join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}