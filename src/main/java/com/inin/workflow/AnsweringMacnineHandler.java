package com.inin.workflow;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.log4j.Logger;

public class AnsweringMacnineHandler implements JavaDelegate {

	private Logger logger = Logger.getLogger("com.inin.workflow");
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {
    	logger.info("Entering AnsweringMacnineHandler");
    	HomeController.addOutputLine("Entering", "AnsweringMacnineHandler.execute");
    	execution.setVariable("wrapupCode", "Answering Machine");
	}

}
