package com.inin.workflow;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.log4j.Logger;

public class DispositionCall implements JavaDelegate {

	private Logger logger = Logger.getLogger("com.inin.workflow");
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {
    	logger.info("Entering DispositionCall");
    	HomeController.addOutputLine("Call dispositioned with wrapup code `" + execution.getVariable("wrapupCode") + "\'", "DispositionCall.execute");
    	HomeController.addOutputLine("Workflow terminated successfully", "DispositionCall.execute");
	}

}
