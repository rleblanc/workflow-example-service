package com.inin.workflow;

import java.util.Random;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.log4j.Logger;

public class HandleCall implements JavaDelegate {
	private Logger logger = Logger.getLogger("com.inin.workflow");
	private Random generator = new Random();
	
	static private String[] wrapupCode =
		{
			"Success", "Failure", "Wrong party"
		};
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {
    	logger.info("Entering HandleCall");
    	HomeController.addOutputLine("Talking with contact", "HandleCall.execute");
    	
    	// Save a random wrapup code so that the next task in the process can read it and display it.
    	final String wrapup = new String(wrapupCode[Math.abs(generator.nextInt() % 3)]);
    	HomeController.addOutputLine("Saving wrapup code: " + wrapup, "HandleCall.execute");
    	execution.setVariable("wrapupCode", wrapup);
    	
    	HomeController.addOutputLine("Hanging up", "HandleCall.execute");
	}

}
