package com.inin.workflow;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.log4j.Logger;

public class LivePerson implements JavaDelegate {

	private Logger logger = Logger.getLogger("com.inin.workflow");
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {
    	logger.info("Entering LivePerson");
    	HomeController.addOutputLine("Entering", "LivePerson.execute");
	}

}
