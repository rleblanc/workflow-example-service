package com.inin.workflow;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.log4j.Logger;

public class PlayWaveFile implements JavaDelegate {

	private Logger logger = Logger.getLogger("com.inin.workflow");
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {
    	logger.info("Entering PlayWaveFile");
    	HomeController.addOutputLine("Entering", "PlayWaveFile.execute");
	}

}
