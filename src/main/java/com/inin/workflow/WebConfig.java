package com.inin.workflow;

import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.converter.HttpMessageConverter;
// import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableAspectJAutoProxy
@EnableWebMvc
@ComponentScan(basePackages = {"com.inin.workflow"})
public class WebConfig extends WebMvcConfigurerAdapter {

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
            ObjectMapper om = new ObjectMapper();
            om.setSerializationInclusion(Inclusion.NON_NULL);
            
            // The following lines are necessary when running a web service accessed by other web services using
            // json as their underlying communication language.
            // But for this demo, we want to return html
            
            // MappingJacksonHttpMessageConverter converter = new MappingJacksonHttpMessageConverter();
            // converter.setObjectMapper(om);
            // converters.add(converter);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler("/**").addResourceLocations("/");
    }

    /*
     *
     * This is where the thread pool executor for the async tasks is configured. This defaults to a
     * SimpleAsyncTaskExecutor if not overridden.
     *
     */
    @Override
    public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
    	ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    	executor.setMaxPoolSize(5);
    	executor.setCorePoolSize(3);
    	executor.initialize();
    	configurer.setTaskExecutor(executor);
    }
}
