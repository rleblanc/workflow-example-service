package com.inin.workflow;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.log4j.Logger;

import java.util.Random;

public class CallAnalysis implements JavaDelegate {
	private Random generator = new Random();

	private Logger logger = Logger.getLogger("com.inin.workflow");
	
	static private String[] callTypes =
		{
			"noAnswer", "isBuzy", "isAnsweringMachine", "isLivePerson"
		};
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {
    	// Randomly choose a call type
    	final String result = new String(callTypes[Math.abs(generator.nextInt()) % 4]);
    	
    	logger.info("Call analysis result is " + result);
    	HomeController.addOutputLine("Evaluating call type to " + result, "CallAnalysis.execute");
    	
    	// Set the variable in the process
    	execution.setVariable("callType", result);
	}

}
