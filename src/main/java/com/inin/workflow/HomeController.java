package com.inin.workflow;

import java.util.List;
import java.util.concurrent.Callable;

// 
import org.apache.log4j.Logger;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class HomeController {

	private Logger logger = Logger.getLogger("com.inin.workflow");
	
	private ProcessEngine processEngine;
	private RepositoryService repositoryService;
	static private String output = new String();
	
	public boolean validProcessEngine() {
        // Initialize the core object that will give us access to all the other activiti API objects
        // The configuration details such as the DB type, address and credentials or
        // mail server host and etc. live in an xml file sitting somewhere in the class path
		if (processEngine == null) {
	        processEngine = ProcessEngines.getDefaultProcessEngine();
	        if (processEngine == null)
	        {
	            logger.error("Unable to get process engine. "
	            		+ "It's most likely because activiti.cfg.xml cannot be found in the class path "
	            		+ "or something went wrong with the contents of that file.");
	            return false;
	        }

	        logger.info("Successfully initialized process engine.");
			
	        // Create a deployment given a MPMN 2.0 XML file
	        // Of course, in this case, the file is static but in a real deployment, we could generate that XML file on the fly.
	        // Note also that one XML file contains one deployment but each deployment can contain several processes.
	        // In real life, a deployment could contain a policy set or all the policy sets of a given customer. TBD
	        repositoryService = processEngine.getRepositoryService();
	        
	        // Read the definition of the process from the XML file
	        repositoryService.createDeployment()
	            .addClasspathResource("diagrams/DialerCall.bpmn20.xml")
	            .deploy();
		}

        return true;
	}

    /**
     * Hitting the home web page starts a new instance of the process.
     * In this context, we're talking about a business process, not an OS process.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public Callable<String> home() {
    	logger.info("Starting Default Request on thread: " + Thread.currentThread().getName());

    	if (validProcessEngine()) {
        	startProcess();
    	}

    	return new Callable<String>() {

    		@Override
    		public String call() throws Exception {
    			logger.info("Returning response on thread: " + Thread.currentThread().getName());
    			return flush();
    		}
    	};
    }

    @ExceptionHandler
    @ResponseBody
    public String handleException(Exception ex) {
            logger.error("Exception: " + ex);
            
            addOutputLine("Error: " + ex.getMessage(), "HomeController.handleException");
			return flush();
    }

    private void startProcess()
    {
        ProcessDefinitionQuery processDefQuery = repositoryService.createProcessDefinitionQuery();
        List<ProcessDefinition> processDefs = processDefQuery.list();
        logger.debug("Number of process definitions: " + processDefQuery.count());
        for (ProcessDefinition processDef: processDefs)
        {
            logger.debug("Process definitions: " + processDef.toString());
            logger.debug("Category: " + processDef.getCategory() +
                               " deployment ID : " + processDef.getDeploymentId() +
                               " ID: " + processDef.getId() +
                               " key:" + processDef.getKey() +
                               " version: " + processDef.getVersion());
        }
        
        // Start that process. AKA create a process instance.
        RuntimeService runtimeService = processEngine.getRuntimeService();
        runtimeService.startProcessInstanceByKey("DialerCall");
        
        // Verify that we started a new process instance
        logger.debug("Number of process instances: " + runtimeService.createProcessInstanceQuery().count());
    }
    
    static public void addOutputLine(String newLine, String method) {
    	output += "<p>" + method + ": " + newLine + "</p>";
    }
    
    private String flush() {
		final String retval = output;
		output = "";
		return retval;
    }
}
